import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CharactersComponent } from './components/characters/characters.component';
import { CharactersDetailsComponent } from './components/characters-details/characters-details.component';
import { CategoriesComponent } from './components/categories/categories.component';

const routes: Routes = [
  { path: 'characters', component: CharactersComponent },
  { path: 'characters-details/:id', component: CharactersDetailsComponent },
  { path: 'categories', component: CategoriesComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
