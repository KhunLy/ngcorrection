import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Md5 } from 'ts-md5';

@Injectable()
export class MarvelInterceptor implements HttpInterceptor {

  constructor() {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<any>> {
    if(request.url.includes(environment.MARVEL_BASE_URL)){
      let clone = this.addCredentials(request);
      return next.handle(clone);
    }
    return next.handle(request);
  }

  addCredentials(request: HttpRequest<unknown>) : HttpRequest<unknown>{

    let md5 = new Md5();
    let ts = new Date().getTime();
    let hash = md5.appendStr(ts+environment.MARVEL_PRI_KEY+environment.MARVEL_PUB_KEY).end();

    let clone = request.clone({ params: request.params.set('ts', ts.toString()) });
    let clone1 = clone.clone({ params: clone.params.set('apikey', environment.MARVEL_PUB_KEY) });
    let clone2 = clone1.clone({ params: clone1.params.set('hash', hash.toString()) });
    return clone2;
  }
}
