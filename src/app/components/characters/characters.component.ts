import { Component, OnInit } from '@angular/core';
import { CharacterService } from '../../services/character.service';
import { Characters } from 'src/app/models/characters';

@Component({
  selector: 'app-characters',
  templateUrl: './characters.component.html',
  styleUrls: ['./characters.component.scss']
})
export class CharactersComponent implements OnInit {

  model: Characters

  constructor(private characterService: CharacterService) { }

  ngOnInit(): void {
    this.characterService.context
      .subscribe(data => {
        this.model = data;
      });
  }

}
