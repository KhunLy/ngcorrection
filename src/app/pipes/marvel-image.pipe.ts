import { Pipe, PipeTransform } from '@angular/core';
import { Thumbnail } from '../models/characters';

@Pipe({
  name: 'marvelImage'
})
export class MarvelImagePipe implements PipeTransform {

  transform(value: Thumbnail, ...args: unknown[]): string {
    return value.path + '/' + args[0] +'.' + value.extension;
  }

}
