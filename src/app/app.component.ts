import { Component, OnInit } from '@angular/core';
import { NbMenuItem } from '@nebular/theme';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  items: NbMenuItem[]

  constructor() {}
  ngOnInit(): void {
    this.items = [
      { title: 'Personnages', link: '/characters', icon: 'sun' },
      { title: 'Categories', link: '/categories', icon: 'book' },
    ];
  }
}
