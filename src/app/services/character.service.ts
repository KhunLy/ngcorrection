import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Characters } from '../models/characters';
import { map } from 'rxjs/operators';
 
@Injectable({
  providedIn: 'root'
})
export class CharacterService {

  context: BehaviorSubject<Characters>

  constructor(private http: HttpClient) {
    this.context = new BehaviorSubject<Characters>(null);
    this.refreshContext();
  }

  refreshContext() :void {
    this.http.get<Characters>(environment.MARVEL_BASE_URL + 'characters?limit=100')
    .subscribe(data => {
      console.log(data);
      this.context.next(data);
    });
  }

  getDetails(id:number) : Observable<Characters> {
    return this.http.get<Characters>(environment.MARVEL_BASE_URL + 'characters/' + id)
  }
}
