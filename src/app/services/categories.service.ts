import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Category } from '../models/category';
import { BehaviorSubject } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { environment } from '../../environments/environment';
 
@Injectable({
  providedIn: 'root'
})
export class CategoriesService {

  context: BehaviorSubject<Category[]>;

  private URL: string = environment.MOVIE_BASE_URL + "Category/";

  constructor(private http: HttpClient) 
  { 
    this.context = new BehaviorSubject<Category[]>(null);
    this.refresh();
  }

  refresh() {
    this.http.get<Category[]>(this.URL).subscribe(data => {
      this.context.next(data);
    });
  }

  add(model : Category) {
    return this.http.post(this.URL, model).pipe(finalize(() => {
      this.refresh();
    }));
  }

  delete(id: number) {
    return this.http.delete(this.URL + id).pipe(finalize(() => {
      this.refresh();
    }));
  }

  update(model : Category) {
    return this.http.put(this.URL, model).pipe(finalize(() => {
      this.refresh();
    }));
  }

  getDetails(id: number) {
    return this.http.get<Category>(this.URL + id);
  }
}
